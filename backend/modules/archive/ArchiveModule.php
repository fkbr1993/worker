<?php

namespace archive;

/**
 * Class ArchiveModule
 *
 * @package archive
 *
 * Archive with work screenshots
 */
class ArchiveModule extends \yii\base\Module
{
    public $controllerNamespace = 'archive\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
