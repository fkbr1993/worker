<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model archive\models\Archive */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Archives', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$attributes = [
	'id',
	'user_id',
	/*'amount',*/
	'description:ntext',
	'time',
	[
		'label' => 'Статус',
		'value' => $model->status == \archive\models\Archive::STATUS_CHECKED ? 'Проверен' : 'Не проверен',
	],
	'file_id',
	[
		'label'  => 'Откр./Закр.',
		'value'  => $model->is_opened ? 'Открыт' : 'Закрыт',
	]
];
if ($model->is_opened){
	$attributes = array_merge([
		[
			'label' => 'Просмотр изображений',
			'value' => '<a href="'.\yii\helpers\Url::toRoute(['/archive/archive/view-images?id=' . $model->id]).'">Перейти</a>',
			'format' => 'raw']
	], $attributes);
}
?>
<div class="archive-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ]) ?>

</div>
