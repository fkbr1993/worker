<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model archive\models\Archive */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="archive-form">

	<?php
	echo Html::errorSummary(
		$model,
		[
			'class' => 'alert alert-danger'
		]
	);

	$form = \kartik\form\ActiveForm::begin([
		'enableClientValidation' => false,
		'options' => [
			'enctype' => 'multipart/form-data'
		]
	]);

	$rows = $model->prepareForm();

	echo \kartik\builder\Form::widget(
		[
			'model' => $model,
			'form' => $form,
			'attributes' => $rows,
		]
	);
	?>

	<div class="form-group">
		<?= Html::resetButton('Сросить', ['class' => 'btn btn-default']) ?>
		<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php \kartik\form\ActiveForm::end(); ?>

</div>
