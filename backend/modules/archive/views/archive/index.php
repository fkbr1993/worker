<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel archive\models\ArchiveSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Архивы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="archive-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить архив', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\Column'],

            'id',
            'user_id',
            /*'amount',*/
            'description:ntext',
            'time',
	        [
		        'attribute' => 'status',
		        'format' => 'raw',
		        'filter' => Html::activeDropDownList(
			        $searchModel,
			        'status',
			        $searchModel->getStatuses(),
			        ['class'=>'form-control','prompt' => 'Все']),
		        'value' => function ($model) {
			        return '<div>'.$model->getStatus().'</div>';
		        },
	        ],
            [
	            'attribute' => 'is_opened',
	            'format' => 'raw',
	            'value' => function($model){
	                if ($model->is_opened){
		                return '<a href='.\yii\helpers\Url::toRoute(['/archive/archive/close?id='.$model->id]).'>Закрыть</a>';
	                }
		            return '<a href='.\yii\helpers\Url::toRoute(['/archive/archive/open?id='.$model->id]).'>Открыть</a>';
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
