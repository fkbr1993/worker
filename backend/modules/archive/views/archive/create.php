<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model archive\models\Archive */

$this->title = 'Добавить архив';
$this->params['breadcrumbs'][] = ['label' => 'Архивы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="archive-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
