<?php
/**
 * Author: alfred
 * Email: alfred
 * @var $images array
 * @var $archive \archive\models\Archive
 */

use \yii\helpers\Html;

$this->title = 'Архив №' . $archive->id;
$this->params['breadcrumbs'][] = ['label' => 'Archives', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->view->registerJsFile('/js/jquery.min.js');
Yii::$app->view->registerJsFile('/js/imagelightbox.min.js');
Yii::$app->view->registerJsFile('/js/application.js');

?>

<div class="archive-view">
	<h3>Описание:</h3>
	<p><?= $archive->description ? $archive->description : 'Отсутствует'; ?></p>
	<h3>Всего фото: <?= count($images); ?></h3>
	<h3>Отработано времени: <?= floor(count($images)/60) . ' ч.  ' . (count($images) - floor(count($images)/60)*60) . ' мин.'; ?></h3>
	<div id="slider">
		<ul class="bjqs">
			<?php
			$cnt = 0;
			foreach ($images as $image){
				$source = $archive->getPathTodir(false) . DIRECTORY_SEPARATOR . $image; ?>
				<li>
					<strong>#<?= ++$cnt; ?></strong> <?= $image; ?> <br />
					<a href="<?= $source; ?>" class="images-group">
						<img src="<?= $source;?>" width="400" height="300" alt="#<?= $cnt; ?> <?= $image; ?>">
					</a>
				</li>
			<?php } ?>
			<li>
				<h2><a href="<?= \yii\helpers\Url::toRoute(['/archive/archive/close?id='.$archive->id . '&checked=1'])?>">Проверено</a></h2>
			</li>
		</ul>
	</div>

</div>
