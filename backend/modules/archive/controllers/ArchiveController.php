<?php

namespace archive\controllers;

use Yii;
use archive\models\Archive;
use archive\models\ArchiveSearch;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ArchiveController implements the CRUD actions for Archive model.
 */
class ArchiveController extends Controller
{
	public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Archive models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArchiveSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Archive model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Archive model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Archive();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Archive model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Archive model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Archive model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Archive the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Archive::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	/**
	 * @param $id
	 *
	 * @return \yii\web\Response
	 * @throws Exception
	 */
	public function actionOpen($id){
		$zip = new \ZipArchive();
		$archive = Archive::find()->where(['id' => $id])->one();
		if (!$archive){
			throw new Exception('Архив не найден', 500);
		}
		$res = $zip->open($archive->getPathToFile());
		if ($res){
			if (file_exists($archive->getPathToDir()))
				throw new Exception('Архив уже открыт ' . $archive->getPathToDir(), 500);
			$res = $zip->extractTo($archive->getPathToDir());
			if ($res){
				$archive->is_opened = 1;
				$archive->save(false);
			} else {
				throw new Exception('Не удалось извлечь данные в папку ' . $archive->getPathToDir(), 500);
			}
			$zip->close();
		} else {
			throw new Exception('Невозможно открыть архив', 500);
		}
		return $this->redirect(['/archive/archive/view?id='.$id]);
	}

	/**
	 * @param $id
	 *
	 * @return \yii\web\Response
	 * @throws Exception
	 */
	public function actionClose($id){
		$archive = Archive::find()->where(['id' => $id])->one();
		if (!$archive){
			throw new Exception('Архив не найден', 500);
		}
		if (!file_exists($archive->getPathToDir())){
			throw new Exception('Данных не существует в папке ' . $archive->getPathToDir(), 500);
		}
		$it = new \RecursiveDirectoryIterator($archive->getPathToDir(), \RecursiveDirectoryIterator::SKIP_DOTS);
		$files = new \RecursiveIteratorIterator($it,
			\RecursiveIteratorIterator::CHILD_FIRST);
		foreach($files as $file) {
			if ($file->isDir()){
				rmdir($file->getRealPath());
			} else {
				unlink($file->getRealPath());
			}
		}
		rmdir($archive->getPathToDir());
		$archive->is_opened = 0;
		if (!empty($_GET['checked'])){
			$archive->status = Archive::STATUS_CHECKED;
		}
		$archive->save(false);
		return $this->redirect(['/archive/archive/view?id='.$id]);
	}

	/**
	 * @param $id
	 *
	 * @throws Exception
	 */
	public function actionViewImages($id){
		$archive = Archive::find()->where(['id' => $id])->one();
		if (!$archive){
			throw new Exception('Архив не найден', 500);
		}
		if (!$archive->is_opened){
			throw new Exception('Архив не открыт', 500);
		}
		if (!file_exists($archive->getPathToDir())){
			throw new Exception('Данных не существует в папке ' . $archive->getPathToDir(), 500);
		}
		$images = scandir($archive->getPathTodir());
		unset($images[0]);
		unset($images[1]);

		return $this->render('view-images', ['images' => $images, 'archive' => $archive, ]);
	}
}
