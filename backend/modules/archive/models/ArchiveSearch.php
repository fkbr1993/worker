<?php

namespace archive\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use archive\models\Archive;

/**
 * ArchiveSearch represents the model behind the search form about `archive\models\Archive`.
 */
class ArchiveSearch extends Archive
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'amount', 'status', 'file_id'], 'integer'],
            [['description', 'time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {


        $query = Archive::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
	        'sort' => [
		        'attributes' => ['id', 'time', 'status', 'amount'],
		        'defaultOrder' => [
			        'id' => SORT_DESC
		        ],
	        ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'amount' => $this->amount,
            'time' => $this->time,
            'status' => $this->status,
            'file_id' => $this->file_id,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
