<?php

namespace archive\models;

use kartik\builder\Form;
use metalguardian\fileProcessor\behaviors\UploadBehavior;
use metalguardian\fileProcessor\helpers\FPM;
use user\models\User;
use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "archive".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $amount
 * @property string $description
 * @property string $time
 * @property integer $status
 * @property integer $file_id
 *
 * @property User $user
 */
class Archive extends \yii\db\ActiveRecord
{
	const STATUS_NOT_CHECKED = 0;
	const STATUS_CHECKED = 1;

	/**
	 * @return array
	 */
	public static function getStatuses()
	{
		return [
			self::STATUS_CHECKED => 'Проверен',
			self::STATUS_NOT_CHECKED => 'Не проверен',
		];
	}

	/**
	 * @return null
	 */
	public function getStatus()
	{
		$array = self::getStatuses();
		return isset($array[$this->status]) ? $array[$this->status] : null;
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(
			parent::behaviors(),
			[
				'file' => [
					'class' => UploadBehavior::className(),
					'attribute' => 'file_id',
					'image' => false,
					'validator' => ['extensions' => 'zip', ],
					'required' => true,
				]
			]
		);
	}

	/**
	 * @inheritdoc
	 */
	public function beforeDelete()
	{
		parent::beforeDelete();

		FPM::deleteFile($this->file_id);

		return true;
	}

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'archive';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'amount', 'status', ], 'integer'],
            [['description'], 'string'],
            [['is_open', ], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ID Сотрудника',
            'amount' => 'Всего фото',
            'description' => 'Описание',
            'time' => 'Время загрузки',
            'status' => 'Статус',
            'file_id' => 'Архив',
            'is_opened' => 'Откр./Закр.',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

	/**
	 * @return array
	 */
	public function prepareForm($viewAction = false)
	{
		return $viewAction ? [
			'id',
			'user_id',
			/*'amount',*/
			'description',
			[
				'attribute' => 'Архив',
				'format' => 'raw',
				'value' => $this->file_id
					? Html::a(
						'Скачать',
						\metalguardian\fileProcessor\helpers\FPM::originalSrc($this->file_id)
					)
					: null,
			],
			'status',
			'time',
		] : [
			'user_id' => [
				'type' => Form::INPUT_TEXT,
			],
			'description' => [
				'type' => Form::INPUT_TEXTAREA,
			],
				'file_id' => [
					'label' => 'Архив',
					'type' => \kartik\builder\Form::INPUT_FILE,
				],
			'filePreview' => [
				'type' => \kartik\builder\Form::INPUT_RAW,
				'value' => $this->isNewRecord == 'create'
					? null
					: Html::a(
						'Скачать ' . \metalguardian\fileProcessor\helpers\FPM::getOriginalFileName($this->file_id, 'archive', 'zip'),
						\metalguardian\fileProcessor\helpers\FPM::originalSrc($this->file_id)
					)
			],
			'status' => [
				'type' => Form::INPUT_DROPDOWN_LIST,
				'items' => $this->getStatuses(),
			],
			'time' => [
				'type' => Form::INPUT_TEXT,
			],
		];
	}

	/**
	 * @return string
	 */
	public function getPathToFile(){
		$path = $_SERVER['DOCUMENT_ROOT'];
		$file = FPM::originalSrc($this->file_id);
		if (file_exists($path . $file))
			return $path . $file;
		else
			throw new Exception('Архив не найден', 500);
	}

	/**
	 * @return string
	 */
	public function getPathTodir($absolute = true){
		$path = $absolute ? $_SERVER['DOCUMENT_ROOT'] : '';

		$dir = '/uploads/archive-'.$this->id;
		return $path . $dir;
	}
}
