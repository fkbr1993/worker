<?php

namespace user;

/**
 * Class UserModule
 *
 * @package app\modules\user
 *
 * Main Class of application, actually a programmer
 */
class UserModule extends \yii\base\Module
{
    public $controllerNamespace = 'user\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
