<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel user\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сотрудники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить сотрудника', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\Column'],

            'id',
	        'image_id',
            'username',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
	        'name',
            'email:email',
	        'phone',
	        //'second_phone',
	        'hours_per_week',
	        [
		        'attribute' => 'status',
		        'format' => 'raw',
		        'filter' => Html::activeDropDownList(
			        $searchModel,
			        'status',
			        $searchModel->getStatuses(),
			        ['class'=>'form-control','prompt' => 'Все']),
		        'value' => function ($model) {
			        return '<div>'.$model->getStatus().'</div>';
		        },
	        ],
	        //'created_at',
	        //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
