<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model user\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<?php print Html::errorSummary(
$model,
[
'class' => 'alert alert-danger'
]
); ?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?php // print $form->field($model, 'auth_key')->textInput(['maxlength' => 32]) ?>

    <?php //print $form->field($model, 'password_hash')->textInput(['maxlength' => 255]) ?>

    <?php // print $form->field($model, 'password_reset_token')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>

    <?php print $model->isNewRecord ? $form->field($model, 'password')->textInput(['maxlength' => 255]) : ''; ?>

	<?= $form->field($model, 'status')->dropDownList($model::getStatuses()) ?>

    <?php //print $form->field($model, 'created_at')->textInput() ?>

    <?php //print $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'second_phone')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'hours_per_week')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
