<?php

namespace user\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property integer $image_id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $name
 * @property string $phone
 * @property string $second_phone
 * @property integer $hours_per_week
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends \yii\db\ActiveRecord
{
	static $isSearching = false;

	/**
	 * Password for new users
	 * @var string
	 */
	public $password;

	const STATUS_NOT_ACTIVE= 0;
	const STATUS_ACTIVE = 1;

	/**
	 * @return array
	 */
	public static function getStatuses()
	{
		return [
			self::STATUS_ACTIVE=> 'Активен',
			self::STATUS_NOT_ACTIVE=> 'Не активен',
		];
	}

	/**
	 * @return null
	 */
	public function getStatus()
	{
		$array = self::getStatuses();
		return isset($array[$this->status]) ? $array[$this->status] : null;
	}

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'name', 'auth_key', 'password_hash', 'email', 'phone'], 'required'],
	        [['password', ], 'validatePassword'],
            [['status', 'created_at', 'updated_at', 'hours_per_week', 'image_id'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'name', 'phone', 'second_phone'], 'string', 'max' => 255],
	        [['username', 'email', ], 'unique'],
            [['auth_key'], 'string', 'max' => 32]
        ];
    }


	/**
	 * @param $attribute
	 * @param $params
	 *
	 * @return bool
	 */
	public function validatePassword($attribute, $params){
		if ($this->isNewRecord && empty($this->password)){
			$this->addError($attribute, 'Необходимо заполнить «Пароль»');
			return false;
		}
		return true;
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image_id' => 'Изображение',
            'username' => 'Логин',
            'auth_key' => 'Auth Key',
	        'password_hash' => 'Password Hash',
	        'password_reset_token' => 'Password Reset Token',
	        'email' => 'Email',
	        'status' => 'Статус',
	        'created_at' => 'Создан',
	        'updated_at' => 'Обновлен',
	        'name' => 'ФИО',
	        'phone' => 'Телефон',
            'second_phone' => 'Запасной телефон',
            'hours_per_week' => 'Неделя',
            'password' => 'Пароль',
        ];
    }

	/**
	 * @return bool
	 * @throws \yii\base\Exception
	 * @throws \yii\base\InvalidConfigException
	 */
	public function beforeValidate(){
		if ($this->isNewRecord && !self::$isSearching){
			$this->auth_key = Yii::$app->security->generateRandomString();
			$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
			$this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
		}
		return parent::beforeValidate();
	}

	public function beforeSave($insert){
		if ($this->isNewRecord)
			$this->created_at=time();
		$this->updated_at = time();
		return parent::beforeSave($insert);
	}
}
