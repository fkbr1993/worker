<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
	'language' => 'ru',
	'aliases' => [
		'configuration' => realpath(
			__DIR__ . DIRECTORY_SEPARATOR . '..' .
			DIRECTORY_SEPARATOR . 'modules' .
			DIRECTORY_SEPARATOR . 'configuration'
		),
		'user' => realpath(
			__DIR__ . DIRECTORY_SEPARATOR . '..' .
			DIRECTORY_SEPARATOR . 'modules' .
			DIRECTORY_SEPARATOR . 'user'
		),
		'archive' => realpath(
			__DIR__ . DIRECTORY_SEPARATOR . '..' .
			DIRECTORY_SEPARATOR . 'modules' .
			DIRECTORY_SEPARATOR . 'archive'
		),
	],
    'modules' => [
	    'configuration' => [
		    'class' => 'configuration\Configuration'
	    ],
	    'user' => [
		    'class' => 'user\UserModule'
	    ],
	    'archive' => [
		    'class' => 'archive\ArchiveModule'
	    ],
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
