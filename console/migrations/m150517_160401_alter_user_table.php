<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m150517_160401_alter_user_table
 * add some fields
 */
class m150517_160401_alter_user_table extends Migration
{
	public $tableName = '{{user}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
	    $this->addColumn($this->tableName, 'name', 'VARCHAR(255) COMMENT "ФИО"');
	    $this->addColumn($this->tableName, 'phone', 'VARCHAR (255) NOT NULL COMMENT "Телефон"');
	    $this->addColumn($this->tableName, 'second_phone', 'VARCHAR (255) COMMENT "Запасной телефон"');
	    $this->addColumn($this->tableName, 'hours_per_week', 'INT(2) COMMENT "Отработка в неделю (часов)"');
	    $this->addColumn($this->tableName, 'image_id', 'INT(10) COMMENT "Изображение"');
    }
    
    public function safeDown()
    {
	    $this->dropColumn($this->tableName, 'name');
	    $this->dropColumn($this->tableName, 'phone');
	    $this->dropColumn($this->tableName, 'second_phone');
	    $this->dropColumn($this->tableName, 'hours_per_week');
	    $this->dropColumn($this->tableName, 'image_id');
    }

}
