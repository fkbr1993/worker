<?php

use yii\db\Schema;
use yii\db\Migration;

class m150922_133645_alter_archive_table extends Migration
{
    public function up()
    {
		$this->addColumn('{{%archive}}', 'is_opened', Schema::TYPE_SMALLINT);
    }

    public function down()
    {
	    $this->dropColumn('{{%archive}}', 'is_opened');
    }
}
