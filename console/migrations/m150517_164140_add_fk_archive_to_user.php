<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m150517_164140_add_fk_archive_to_user
 */
class m150517_164140_add_fk_archive_to_user extends Migration
{
    public $tableName = '{{archive}}';
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
	    $this->addForeignKey('fk_archive_to_user', $this->tableName, 'user_id', '{{user}}', 'id', null, 'CASCADE');
    }
    
    public function safeDown()
    {
	    $this->dropForeignKey('fk_archive_to_user', $this->tableName);
    }
}
