<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m150517_163230_create_archive_tables
 * Create table for archives with work screenshots
 */
class m150517_163230_create_archive_tables extends Migration
{
	public $tableName = '{{archive}}';
    

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
	    $this->createTable($this->tableName, [
		    'id' => Schema::TYPE_PK,
		    'user_id' => Schema::TYPE_INTEGER,
		    'amount' => Schema::TYPE_INTEGER,
		    'description' => Schema::TYPE_TEXT,
		    'time' => Schema::TYPE_TIMESTAMP,
		    'status' => Schema::TYPE_INTEGER,
		    'file_id' => Schema::TYPE_INTEGER,
	    ], $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB' );
    }
    
    public function safeDown()
    {
	    $this->dropTable($this->tableName);
    }

}
