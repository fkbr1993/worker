<?php

namespace app\modules\archive;

class ArchiveModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\archive\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
