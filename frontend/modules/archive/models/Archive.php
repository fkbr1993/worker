<?php

namespace archive\models;

use metalguardian\fileProcessor\behaviors\UploadBehavior;
use metalguardian\fileProcessor\helpers\FPM;
use user\models\User;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "archive".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $amount
 * @property string $description
 * @property string $time
 * @property integer $status
 * @property integer $file_id
 *
 * @property User $user
 */
class Archive extends \yii\db\ActiveRecord
{
	const STATUS_NOT_CHECKED = 0;
	const STATUS_CHECKED = 1;

	/**
	 * @return array
	 */
	public static function getStatuses()
	{
		return [
			self::STATUS_CHECKED => 'Проверен',
			self::STATUS_NOT_CHECKED => 'Не проверен',
		];
	}

	/**
	 * @return null
	 */
	public function getStatus()
	{
		$array = self::getStatuses();
		return isset($array[$this->status]) ? $array[$this->status] : null;
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return ArrayHelper::merge(
			parent::behaviors(),
			[
				'file' => [
					'class' => UploadBehavior::className(),
					'attribute' => 'file_id',
					'image' => false,
					'validator' => ['extensions' => 'zip', ],
					'required' => true,
				]
			]
		);
	}

	/**
	 * @inheritdoc
	 */
	public function beforeDelete()
	{
		parent::beforeDelete();

		FPM::deleteFile($this->file_id);

		return true;
	}

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'archive';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'amount', 'status', 'file_id'], 'integer'],
            [['description'], 'string'],
            [['time'], 'safe']
        ];
    }

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'user_id' => 'ID Сотрудника',
			'amount' => 'Всего фото',
			'description' => 'Описание деятельности (4-6 предложений)',
			'time' => 'Время загрузки',
			'status' => 'Статус',
			'file_id' => 'Архив',
		];
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
