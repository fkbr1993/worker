<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
	    <?php if (!Yii::$app->user->isGuest) {?>
		    <div id="topMenu">
			    <ul class="topMenu">
				    <!--<li><a href="<?php /*print \yii\helpers\Url::to(['site/changePassword']);*/?>">Cменить пароль</a></li>-->
				    <li><a href="<?php print \yii\helpers\Url::to(['site/logout']);?>">Выйти (<?php print Yii::$app->user->identity->username; ?>)</a></li>
			    </ul>
		    </div>
	    <?php } ?>
        <div class="container">
        <?= $content ?>
        </div>
    </div>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
