<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Загрузка архива';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-upload">
	<h1><?= Html::encode($this->title) ?></h1>

	<div class="row">
		<?php
		if (Yii::$app->session->getFlash('archive')) {
			print Yii::$app->session->getFlash('archive');
		} else {

			$form = \kartik\form\ActiveForm::begin(
				[
					'enableClientValidation' => false,
					'options' => [
						'enctype' => 'multipart/form-data'
					]
				]
			);

			$rows = [
				'file_id' => [
					'label' => 'Архив',
					'type' => \kartik\builder\Form::INPUT_FILE,
				],
				'description' => [
					'type' => \kartik\builder\Form::INPUT_TEXTAREA,
				],
			];

			echo \kartik\builder\Form::widget(
				[
					'model' => $model,
					'form' => $form,
					'attributes' => $rows,
				]
			);
			?>

			<div class="form-group">
				<?= Html::submitButton(
					'Сохранить',
					['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
				) ?>
			</div>

			<?php \kartik\form\ActiveForm::end(); ?>
		<?php } ?>
	</div>
</div>
